import numpy as np
import scipy 
import bilby 
import copy

def change_in_prior(result_file, final_priors):
    """
    Function reweighing the posteriors for a 15D or 16D
    run one a change in posteriors. This enables one 
    to reweight the results to some standard prior and
    make for a more consistent comparison

    ARGS:
    -----
    - result_file: the bilby result file obtained 
                   via the unlensed run 
    - final_priors: Bilby prior dict corresponding to
                    the final desired priors

    RETURNS:
    --------
    - log_Z_final: the log evidence values for 
                   the new desired samples
    - samples_final: the samples reweighed to
                     account for the change in priors
    """

    # read the result file and extract the desired information 
    results = bilby.result.read_in_result(filename = result_file)
    old_priors = results.priors.copy()
    # check if the Morse factor should be added to the standard prior
    if 'n_phase' in list(old_priors.keys()) and 'n_phase' not in list(final_priors.keys()):
        final_priors['n_phase'] = copy.copy(old_priors['n_phase'])
    if 'n_phase' not in list(old_priors.keys()) and 'n_phase' in list(final_priors.keys()):
        old_priors['n_phase'] = copy.copy(final_priors['n_phase'])
    if 'geocent_time' not in list(final_priors.keys()) and 'geocent_time' in list(old_priors.keys()):
        old_priors.pop('geocent_time')
    ln_Z_old = results.log_evidence
    final_priors = copy.copy(final_priors)
    samples = results.posterior.copy()
    log_priors = results.posterior['log_prior']
    for key in list(samples.keys()):
        if key not in list(old_priors.keys()):
            samples.pop(key)

    if 'geocent_time' not in final_priors.keys() and 'geocent_time' in old_priors.keys():
        final_priors['geocent_time'] = copy.copy(old_priors['geocent_time'])
    else:
       for key in old_priors.keys():
           if 'time' in key:
               final_priors[key] = copy.copy(old_priors[key])


    sub_samps = [{key : samples[key][i] for key in samples.keys()} 
                 for i in range(len(log_priors))]


    # compute the reweighted evidence
    p_samps_old = np.array([old_priors.prob(sub_samps[i]) for i in range(len(sub_samps))])
    p_samps_new = np.array([final_priors.prob(sub_samps[i]) for i in range(len(sub_samps))])
    weights = p_samps_new/np.exp(log_priors) #p_samps_old
    log_z_final = np.log(np.mean(weights)) + ln_Z_old

    # reweight the samples
    indices = len(weights)
    idxs = np.random.choice(indices, size = len(weights), p = weights/np.sum(weights))
    samples_final = dict()

    for key in samples:
        samples_final[key] = samples[key][idxs]

    return log_z_final, samples_final


def unlensed_to_lensed_result(interferometers, waveform_generator, unlensed_result_file, N = int(1e5), N_samples = None):
    """
    Function converting the products of an unlensed run into a lensed
    run 

    ARGS:
    -----
    - interferometers: the list of Bilby interferometers objects used
                       for the analysis
    - waveform_generator: bilby waveform generator object used
                          for the lensed run
    - unlensed_result_file: the bilby result file obtained from the 
                            unlensed run  
    - N: the number of final reweighted samples to be produced
    - N_samples: the number of samples to compute the lensed information. 
                 If None, the entire set of samples will be used.
    """    
    # read in the lensed results 
    unlensed_result = bilby.result.read_in_result(filename = unlensed_result_file)

    # extract the parameters 
    unlensed_samps_temp = unlensed_result.posterior.copy()
    unl_log_likelis_temp = unlensed_samps_temp['log_likelihood']
    # remove unnecessary parameters
    for key in list(unlensed_samps_temp.keys()):
        if 'log_' in key or 'recalib' in key:
            unlensed_samps_temp.pop(key)

    # read in the unlensed evidence 
    unlensed_log_evidence = unlensed_result.log_evidence

    # take sub samples if needed
    unlensed_samps = dict()
    if N_samples is not None:
        print("INFO: Doing the sub-sampling using %i samples"%(N_samples))
        idxs = np.random.choice(len(unlensed_samps_temp['geocent_time']), size = N_samples)
        for key in unlensed_samps_temp.keys():
            unlensed_samps[key] = np.array(unlensed_samps_temp[key])[idxs]
        unl_log_likelis = np.array(unl_log_likelis_temp)[idxs]
    else:
        unlensed_samps = unlensed_samps_temp.copy()
        unl_log_likelis = unl_log_likelis_temp

    print("INFO: Computing the evidence under the lensed hypothesis")

    # setup the likelihood for the lensed and the unlensed case
    lensed_likelihood = bilby.gw.GravitationalWaveTransient(interferometers = interferometers,
                                                            waveform_generator = waveform_generator)
    likelihoods = np.zeros(3) # array that will contain the likelihood for 
                     # each Morse factor value
    
    for n in [0, 0.5, 1]:
        print("   Computing likelihood for n = %.1f"%n)
        likelihood_n = np.zeros(len(unlensed_samps['geocent_time']))
        for i in range(len(unlensed_samps['geocent_time'])):
            params = dict()
            for key in unlensed_samps.keys():
                params[key] = unlensed_samps[key][i]
            
            params['n_phase'] = n

            lensed_likelihood.parameters = params.copy()

            # compute the numerator and the denominator 
            lens_likeli = lensed_likelihood.log_likelihood()
            likelihood_n[i] = (lens_likeli - (unl_log_likelis[i]+lensed_likelihood.noise_log_likelihood()))

        # compute the log likelihood based on all the samples 
        log_l = scipy.special.logsumexp(likelihood_n - np.log(len(likelihood_n)))
        likelihoods[int(2*n)] = np.exp(log_l)

    # now that we have the likelihood for each n value, we can compute the 
    # unlensed evidence
    lensed_log_evidence = np.log(1./3) + unlensed_log_evidence + scipy.special.logsumexp(likelihoods)

    print("INFO: log(Z_L) = %.5f"%lensed_log_evidence)

    print("INFO: Computing the joint posteriors")

    log_p_ns = [likelihoods[i] - lensed_log_evidence for i in range(len(likelihoods))]

    ns = [0, 0.5, 1]

    n_samples = np.random.choice(ns, p = log_p_ns/np.sum(log_p_ns), size = N)
    print(n_samples)
    # now we need to do the reweighing for all the samples 
    idxs1 = np.random.choice(len(unlensed_samps['geocent_time']), size = N)
    samps = dict()
    for key in unlensed_samps.keys():
        samps[key] = np.array(unlensed_samps[key])[idxs1]
    samps_log_likeli = np.array(unl_log_likelis)[idxs1]

    # compute the weights for each samples 
    weights = np.zeros(N)
    
    for i in range(N):
        params = dict()
        for key in samps.keys():
            params[key] = samps[key][i]
        params['n_phase'] = n_samples[i]
        lensed_likelihood.parameters = params.copy()
        weights[i] = lensed_likelihood.log_likelihood() - (samps_log_likeli[i]+lensed_likelihood.noise_log_likelihood())


    idxs = np.random.choice(N, p = weights/np.sum(weights))

    samples = dict()
    for key in samps.keys():
        samples[key] = samps[key][idxs]
    samples['n_phase'] = n_samples[idxs]

    # return the info 
    out_dict = {'lensed_log_evidence' : lensed_log_evidence,
                'lensed_n' : n_samples,
                'joint_samples' : samples}

    return out_dict
