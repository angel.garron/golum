""" 
Simple script for an example injection
of a realistic SIS lens with GOLUM.
"""

from __future__ import division, print_function 
import bilby
import matplotlib.pyplot as plt
import numpy as np
from gwosc import datasets
import golum
import json 
from golum.pe.likelihood import JointGravitationalWaveTransient

# Set up the directories 
outdir = 'Outdir_SIS_injection'
label = 'SIS_injection'
logger = bilby.core.utils.logger

# Constants
c = 3e8 # m/s
Mpc = 3.086e19 # km
H_0 = 70 # km/s/Mpc

"""
SIS LENS MODEL WITH LENS POTENTIAL 
"""
def psi (x):
    """
    Lens potential at image location.
    
    Parameters
    ----------
    x: float
        x coordinate of image location.

    Returns
    -------
    psi: float
        Lens potential at image location.
    """

    return np.log(np.abs(x))

def sis_lens (theta_e, beta, z_l, z_s):
    """
    Calculates the magnification and time delay for a singular isothermal sphere lens.
    ----------
    Parameters
    ----------
    theta_e : float
        Einstein radius in arcsec.
    beta : float
        Source position in arcsec.
    z_l : float
        Lens redshift.
    z_s : float
        Source redshift.
    ----------
    Returns
    ----------
    mu_1 : float
        Magnification for the first image.
    mu_2 : float
        Magnification for the second image.
    t_d : float
        Time delay in seconds.
    """

    # Conversions
    theta_e = theta_e * np.pi / 180 / 3600 # rad
    beta = beta * np.pi / 180 / 3600 # rad

    # Distances in meters
    D_l = (c / H_0) * (1 - 1 / (1 + z_l)) * Mpc # m
    D_s = (c / H_0) * (1 - 1 / (1 + z_s)) * Mpc # m
    D_ls = np.abs((c / H_0) * (1 / (1 + z_s) - 1 / (1 + z_l)) * Mpc) # m

    # Dimensionless source position
    y = beta / theta_e

    # Dimensionless image positions
    x_1 = y + 1
    x_2 = y - 1
    
    # Magnifications
    mu_1 = 1 + 1/y
    mu_2 = 1 - 1/y

    # Time delay
    t_d_1 = (1 + z_l) / c * (D_l * D_s / D_ls) * theta_e**2 * (np.abs(x_1 - y)/2 - psi(x_1))
    t_d_2 = (1 + z_l) / c * (D_l * D_s / D_ls) * theta_e**2 * (np.abs(x_2 - y)/2 - psi(x_2))

    plt.figure(figsize=(10,10))
    circle = plt.Circle((0,0),radius=1.0,color='k',fill=False)
    plt.gcf().gca().add_artist(circle)
    plt.scatter(y,0,color='crimson',marker='x',s=100)
    plt.scatter(x_1,0,color='darkred',marker='d',s=np.abs(mu_1)*100,alpha=0.5)
    plt.scatter(x_2,0,color='darkred',marker='d',s=np.abs(mu_2)*100,alpha=0.5)
    plt.scatter(0,0,c='k',marker='*',s=1000)
    plt.xlabel(r'$x$',fontsize=20)
    plt.ylabel(r'$y$',fontsize=20)
    plt.xlim(-2,2)
    plt.ylim(-2,2)
    plt.title(r'$\theta_e = {}$ arcsec, $\beta = {}$ arcsec, $z_l = {}$, $z_s = {}$'.format(theta_e*180*3600/np.pi,beta*180*3600/np.pi,z_l,z_s),fontsize=20)
    plt.savefig('sis_lens.png',dpi=300)
    plt.close()

    print("Magnification for the first image: {}".format(mu_1))
    print("Magnification for the second image: {}".format(mu_2))
    print("Time delay for the first image: {}".format(t_d_1))
    print("Time delay for the second image: {}".format(t_d_2))
    
    return mu_1, mu_2, t_d_1, t_d_2

""" SET UP SYSTEM CHARACTERISTICS """
# Lens and source redshifts
z_l = 0.5
z_s = 2.0
# Einstein radius and source position in arcsec
theta_e = 1.0
beta = 0.6

# Calculate magnifications and time delays
mu_1, mu_2, t_d_1, t_d_2 = sis_lens(theta_e, beta, z_l, z_s)

print("Magnifications: ", mu_1, mu_2)
print("Time delays: ", t_d_1, t_d_2)

luminosity_distance = (3e5 / H_0) * (1 - 1 / (1 + z_s))

# create injection parameters for the source system
# note -- only the luminosity distance will be 
# affected by the lensing 
system_params = dict(chirp_mass = 30.0,
                        mass_ratio = 0.8,
                        a_1 = 0.4,
                        a_2 = 0.3,
                        tilt_1 = 0.5,
                        tilt_2 = 1.0,
                        phi_12 = 1.7,
                        phi_jl = 0.3,
                        luminosity_distance = luminosity_distance,
                        theta_jn = 0.4,
                        psi = 0.1,
                        phase = 1.2,
                        geocent_time = 1126259642.413,
                        ra = 45.0,
                        dec = 5.73)

distance = system_params['luminosity_distance']

# create injections for images
injection_1 = system_params.copy()
injection_1['luminosity_distance'] = distance / np.sqrt(np.abs(mu_1))
injection_1['geocent_time'] = injection_1['geocent_time'] + t_d_1
injection_1['n_phase'] = 0

injection_2 = system_params.copy()
injection_2['luminosity_distance'] = distance / np.sqrt(np.abs(mu_2))
injection_2['geocent_time'] = injection_2['geocent_time'] + t_d_2
injection_2['n_phase'] = 0.5

""" WAVEFORM GENERATION """
bilby.core.utils.setup_logger(outdir = outdir, label = label)

# 3 ifos active at the time
ifos_names_1 = ['H1', 'L1', 'V1']
ifos_names_2 = ['H1', 'L1', 'V1']
duration = 4.
post_trigger_duration = 2.
sampling_frequency = 2048.

# waveform generation
conversion = bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
waveform_arguments = dict(waveform_approximant = 'IMRPhenomXPHM', 
                          reference_frequency = 50.,
                          minimum_frequency = 20.)
waveform_generator = bilby.gw.WaveformGenerator(parameter_conversion = conversion, 
                                                frequency_domain_source_model = golum.tools.waveformmodels.lensed_bbh_model, 
                                                waveform_arguments = waveform_arguments,
                                                sampling_frequency = sampling_frequency,
                                                duration = duration)

ifos_1 = bilby.gw.detector.InterferometerList(["H1", "L1", "V1"])
ifos_1.set_strain_data_from_power_spectral_densities(duration = duration, sampling_frequency = sampling_frequency, start_time = injection_1["geocent_time"] - duration + 2.)
ifos_1.inject_signal(waveform_generator = waveform_generator, parameters = injection_1)

ifos_2 = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
ifos_2.set_strain_data_from_power_spectral_densities(duration = duration, sampling_frequency = sampling_frequency, start_time = injection_2["geocent_time"] - duration + 2.)
ifos_2.inject_signal(waveform_generator = waveform_generator, parameters = injection_2)

# save the psd plots and useful stuff for the runs
logger.info("Saving the data pots to {}".format(outdir))
bilby.core.utils.check_directory_exists_and_if_not_mkdir(outdir)
ifos_1.plot_data(outdir = outdir, label = label+"_1")
ifos_2.plot_data(outdir = outdir, label = label+"_2")

# make the prior dictionary for the analysis
delta_t = injection_2['geocent_time'] - injection_1['geocent_time']

priors = bilby.gw.prior.BBHPriorDict(filename = 'injection_prior.prior')
priors['geocent_time'] = bilby.core.prior.Uniform(name = 'geocent_time', minimum = injection_1['geocent_time'] - 0.1, maximum = injection_1['geocent_time'] + 0.1, latex_label = '$t_c$', unit = '$s$')
priors['delta_t'] = bilby.core.prior.Uniform(name = 'delta_t', minimum = delta_t - 0.2, maximum = delta_t + 0.2, latex_label = '$\Delta t$', unit = '$s$')

""" CHECK BEFORE RUNNING THE ANALYSIS """
# set up the likelihood
likelihood_1 = bilby.gw.GravitationalWaveTransient(ifos_1,waveform_generator,priors=priors)
likelihood_2 = bilby.gw.GravitationalWaveTransient(ifos_2,waveform_generator,priors=priors)
likelihood = JointGravitationalWaveTransient(ifos_1,ifos_2,waveform_generator,priors=priors)

# test the log noise evidences
log_noise_evidence_1 = likelihood_1.noise_log_likelihood()
log_noise_evidence_2 = likelihood_2.noise_log_likelihood()
log_noise_evidence = likelihood.noise_log_likelihood()

if log_noise_evidence_1 + log_noise_evidence_2 - log_noise_evidence < 1e-10:
    print("Log noise evidence 1 + 2 - joint is small enough to be negligible, safe to proceed.")
else:
    print("Log noise evidence 1 + 2 - joint is NOT small enough to be negligible.")

# save the log noise evidence in a txt file
with open(outdir + '/log_noise_evidence.txt', 'w') as f:
    f.write('log_noise_evidence_1 = ' + str(log_noise_evidence_1) + '\n')
    f.write('log_noise_evidence_2 = ' + str(log_noise_evidence_2) + '\n')
    f.write('log_noise_evidence = ' + str(log_noise_evidence) + '\n')

""" RUN THE ANALYSIS """
# run the sampler
#result = bilby.run_sampler(likelihood = likelihood, priors = priors, sampler = 'dynesty', 
#						   	 npoints = 2048, naccept = 60, check_point_plot = True, check_point_delta_t = 1800, 
#							 print_method = 'interval-60', sample = 'acceptance-walk', npool = 16, 
#							 outdir = outdir, label = label)
# plot the corner plot
#result.plot_corner()