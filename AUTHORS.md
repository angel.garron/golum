This file lists the names of the authors having contributed (through code or indirectly) to the development of this software. If your name is not listed here and you think it should be, please contact us. 

Gregory Baltus, Sylvia Biscoveanu,  Ángel Garrón, Otto Hannuksela, K. Haris, Justin Janquart, Rico Ka-Lok Lo, David Keitel, Anupreeta More, Laura Uronen, Chris Van Den Broeck, John Veitch, Mick Wright
